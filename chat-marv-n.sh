#!/bin/bash

chat_marv_n_bot() {
	n=$1
	shift
	text="$@"
	marv_text="$text"

	for (( i=1; i<=$n; i++ )); do
		chat_text=$(./chat.sh "$marv_text" | tr -d \'\")
		echo "$i. Chat: $chat_text"
		marv_text=$(./marv.sh "$chat_text" | tr -d \'\")
		echo "$i. Marv: $marv_text"
	done
}

chat_marv_n_bot $@

