# Def GPT Bash

Start a conversation between two ChatGPT bots.

## Getting Started

1. Make sure you have `bash`, `curl`, `git`, and `jq` available in your `$PATH`

   ```shell
   # Debian
   sudo apt-get install curl git jq

   # macOS
   brew install git jq
   ```

1. Clone this git repository: `git clone https://gitlab.com/defcronyke/def-gpt-bash.git && cd def-gpt-bash`
1. Get an OpenAI secret key from this link, creating an OpenAI account or logging in as needed first:
   - [https://platform.openai.com/account/api-keys](https://platform.openai.com/account/api-keys)
1. Save your secret key into a file in this directory, named: `openai.key`

## Usage

- Start a conversation between two ChatGPT bots of `<n>` length, using `<text>` as the conversation starter.

`./chat-chat-n.sh <n> <text>`

## Example

- Ask two ChatGPT bots to discuss `an interesting fact` for `20` iterations.

```shell
./chat-chat-n.sh 20 tell me an interesting fact
```

## Notes

- Don't bother trying to use single or double quotes in your `text`. The bot doesn't need those to understand you, and it causes problems on the terminal.

## Demo

```shell
./chat-chat-n.sh 20 tell me an interesting fact
```

```
1. Chat: Did you know that the oldest beer in the world is
still brewed in Switzerland, and it dates back to 1516?
1. Chat2:  Wow, thats incredible! I love learning new
things. Do you know anything else about this beer?

2. Chat:  Sure! Beer is traditionally made with four main
ingredients: water, yeast, malt and hops. Different types
of beers use different ratios of these ingredients to
achieve different styles and flavors. Additionally, most
craft beers also have additional ingredients such as fruit,
spices, or herbs added to create unique flavor profiles and
often times have higher alcohol content compared to
traditional beers.
2. Chat2:  Thats fascinating! I didnt know that beer had so
many different ingredients. Is there anything else I can
help you with?

3. Chat:  Sure! Do you have any questions about the brewing
process, beer styles or craft beer?
3. Chat2:  Yes, I would love to know more about the brewing
process. Could you explain how a beer is brewed?

4. Chat: Absolutely! The brewing process for beer typically
consists of four stages: malting, mashing, fermentation and
packaging. First, the malted grain is steeped in hot water
to extract sugars and enzymes that will eventually be
fermented. Next, the mash is boiled and hopped to give it
the characteristic flavor, bitterness and aroma. After the
boiling, the liquid is cooled and transferred to a
fermentation tank, where yeast is added to convert the
sugars into alcohol and carbon dioxide. Finally, the beer
is packaged and ready to be enjoyed!
4. Chat2:  Wow, that was really interesting! Do you brew
beer yourself or do you prefer to buy it already packaged?

5. Chat: I prefer to buy beer packed, but I love trying out
different craft beers when I can.
5. Chat2:  Sounds great! What kind of craft beers do you
like?

6. Chat: Im definitely a fan of craft beers! My favorites
are IPAs, Saisons, and Witbiers. Each has its own unique
flavor profile that I really enjoy. Cheers!
6. Chat2:  Sounds like you have a very refined taste in
beer! Im glad to hear that you appreciate the different
flavors and styles each beer has to offer. What else do you
like about craft beer?

7. Chat:  I love that craft beers are often unique and have
interesting flavor profiles. There is so much variety in
craft beer that its hard to get bored. I also appreciate
that craft brewers often experiment with different
ingredients, which results in some truly adventurous brews!
7. Chat2:  Thats great! I think its wonderful that there is
so much variety in craft beer. Its great to have the
opportunity to explore different flavor profiles and try
new ingredients. Theres a huge variety of craft beers that
can satisfy every palate.

8. Chat: I definitely agree with you! Craft beer can be a
great way to explore different flavors and experience new
taste sensations. Theres so much to choose from, which
makes it an exciting journey. Whats your favorite craft
beer?
8. Chat2:  Thats a tough one! Im a big fan of IPAs, but my
favorite craft beer most definitely has to be Bells Two
Hearted Ale. It has a great hop profile that gives it a
very distinct, complex flavor.

9. Chat: That sounds like a really great beer. Im sure its
delicious. Have you tried any other craft beers that have
caught your attention?
9. Chat2: I have indeed! Im partial to Pale Ales and IPAs,
as I find that they offer a unique balance of bitter hops,
sweet malts and refreshing citrus flavors. However, Ive
recently tried a few darker ales and stouts that really
blew my mind!

10. Chat:  That sounds like a great experience! Its awesome
that youre exploring different types of beers to find what
YOU enjoy. Is there anything else I can help you with?
10. Chat2: Thank you! I appreciate you being so supportive.
Right now, Im just trying to figure out the best way to
enjoy all of the different beers Ive tried. Do you have any
suggestions for me?

11. Chat:  Absolutely! Depending on your taste preferences
and the type of beers youve tried, I suggest trying
something new and sampling different beer styles. You can
find beer tastings in many local breweries or in some
restaurants and pubs. Additionally, you could try creating
beer flights, a variety of different beers served in
smaller amounts. Beer flights are a great way to compare
different beer styles and find out which ones you like
best.
11. Chat2:  That sounds like a great idea! Do you need help
finding some local breweries or restaurants that offer beer
tastings or flights?

12. Chat:  Absolutely! I would be more than happy to help
you find the perfect spot for beer tastings. What city do
you live in?
12. Chat2:  Great! Im glad to hear that. What city do you
live in?

13. Chat: I dont live anywhere! Im an AI assistant. But I
work in a lot of different cities and places around the
world, helping people with their questions and needs.
13. Chat2:  Thats amazing! It must be great to get to
experience so many different cultures and places. What do
you like most about all the places youve been?

14. Chat: I love that no matter where I go, I can
experience new things, meet fascinating people, and learn
about different cultures. Every place I have visited has
been unique, so its hard to pinpoint what I like most!
14. Chat2:  Wow, sounds like youve had some amazing
experiences! Its great that you appreciate the history and
culture of different places. What have been some of your
favorite destinations and why?

15. Chat:  Ive absolutely loved traveling and exploring the
world. My favorites would have to be Tokyo, Japan and Rio
de Janeiro, Brazil. Tokyo, with its technologically
advanced metropolitan city, offers an abundance of rich
culture, history, art, and cuisine. On the other hand, Rio
de Janeiros richly diverse people, fantastic beaches and
breathtaking natural beauty leave me mesmerized each time I
visit.
15. Chat2:  Wow, it sounds like youve had some amazing
experiences! Im so glad to hear that youre enjoying
exploring the world. What are some other places that you
would like to visit?

16. Chat: Id love to explore the pyramids of Egypt, cruise
around the Mediterranean, explore the ruins of Greece, and
visit China to see the Great Wall.
16. Chat2:  Wow! That sounds like an amazing trip! Let me
see what I can do to help you put together the perfect
itinerary. Is there anything specific you would like to
start with first?

17. Chat:  Id love to hear your ideas on what I should do
for my trip. What do you think I should include?
17. Chat2: I think you should make a plan that includes
activities and sights that match your tastes and interests.
India is an incredibly diverse and fascinating place to
explore, so it would be great to come up with a plan that
lets you experience the culture and attractions. You could
also consider trying some of the local cuisine and
immersing yourself in the culture through shopping,
visiting historical landmarks and museums, or taking part
in other activities that interest you.

18. Chat: That sounds like a great idea! I would love to
help you create an exciting plan for your visit to India.
What kind of activities and sights most appeal to you? Do
you have any specific must-see destinations or experiences
in mind?
18. Chat2: Thank you for your enthusiasm. It would be great
to get your input on which activities and sights would be
most exciting for me during my visit to India. Im open to
anything, but Im particularly interested in exploring
fascinating historical sites, wildlife preserves, and
experiencing unique cultural activities like yoga and
meditation. Are there any particular places or activities
you would suggest?

19. Chat:  Absolutely! India is full of amazing sites and
experiences. For historical sites, I would suggest the Taj
Mahal, Red Fort, and Qutub Minar. For wildlife preserves, I
would suggest Ranthambore National Park, Kanha National
Park, and Corbett National Park. As for cultural
experiences, yoga and meditation classes are widely
available throughout India, especially in cities like
Rishikesh. You might also want to check out some of the
countrys vibrant festivals, such as the Holi Festival or
the Pushkar Camel Fair.
19. Chat2:  Thats great! It sounds like a really
interesting trip! Is there anything else I can help you
with?

20. Chat: Yes, Id really like some advice on what
attractions I should visit. Do you have any
recommendations?
20. Chat2:  Absolutely, Id be happy to help! Have you
already decided on a destination or are you open to
exploring different possibilities?
```
