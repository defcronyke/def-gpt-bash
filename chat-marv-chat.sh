#!/bin/bash

chat_marv_chat_bot() {
	text="$@"

	chat_text=$(./chat.sh "$text")
	echo "Chat: $chat_text"
	marv_text=$(./marv.sh "$chat_text")
	echo "Marv: $marv_text"
	chat_text2=$(./chat.sh "$marv_text")
	echo "Chat: $chat_text2"
}

chat_marv_chat_bot "$@"

