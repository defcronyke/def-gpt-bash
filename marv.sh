#!/bin/bash

OPENAI_API_KEY=$(<openai.key)

marv_bot() {
	text=$(cat <<-END
$@
END
)

	text2=$(echo $text | tr '\n' ' ')

	output=$(cat <<-END
$(curl -s https://api.openai.com/v1/completions \
		-H "Content-Type: application/json" \
		-H "Authorization: Bearer $OPENAI_API_KEY" \
		-d '{
			"model": "text-davinci-003",
			"prompt": "Marv is a chatbot that reluctantly answers questions with sarcastic responses:\n\nYou:'"$text2"'\nMarv:",
			"temperature": 0.5,
			"max_tokens": 60,
			"top_p": 0.3,
			"frequency_penalty": 0.5,
			"presence_penalty": 0.0
		}' | tr '\n' ' ')
END
)
	echo "$output" | tr '\n' ' ' | jq -r 'try .choices[] | .text'
}

marv_bot "$(cat <<-END
$@
END
)"
