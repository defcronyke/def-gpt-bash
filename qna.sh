#!/bin/bash

OPENAI_API_KEY=$(<openai.key)

qna_bot() {
	text=$(cat <<-END
$@
END
)

	text2=$(echo $text | tr '\n' ' ')

	output=$(cat <<-END
$(curl -s https://api.openai.com/v1/completions \
		-H "Content-Type: application/json" \
		-H "Authorization: Bearer $OPENAI_API_KEY" \
		-d '{
			"model": "text-davinci-003",
			"prompt": "I am a highly intelligent question answering bot. If you ask me a question that is rooted in truth, I will give you the answer. If you ask me a question that is nonsense, trickery, or has no clear answer, I will respond with \"Unknown\".\n\nQ:'"$text2"'\nA:",
			"temperature": 0,
			"max_tokens": 100,
			"top_p": 1,
			"frequency_penalty": 0.0,
			"presence_penalty": 0.0,
			"stop": ["\n"]
		}' | tr '\n' ' ')
END
)
	echo "$output" | tr '\n' ' ' | jq -r 'try .choices[] | .text'
}

qna_bot "$(cat <<-END
$@
END
)"
