#!/bin/bash

chat_chat_n_bot() {
	n=$1
	shift
	text="$@"
	chat2_text=$(cat <<-END
$text
END
)

	for (( i=1; i<=$n; i++ )); do
		chat_text=$(cat <<-END
$(./chat.sh "$chat2_text" | tr -d \'\")
END
)
		echo "$i. Chat: $chat_text"
		chat2_text=$(cat <<-END
$(./chat.sh "$chat_text" | tr -d \'\")
END
)
		echo "$i. Chat2: $chat2_text"
		echo ""
	done
}

chat_chat_n_bot $@
