#!/bin/bash

OPENAI_API_KEY=$(<openai.key)

chat_bot() {
	text=$(cat <<-END
$@
END
)

	text2=$(echo $text | tr '\n' ' ')

	output=$(cat <<-END
$(curl -s https://api.openai.com/v1/completions \
		-H "Content-Type: application/json" \
		-H "Authorization: Bearer $OPENAI_API_KEY" \
		-d '{
			"model": "text-davinci-003",
			"prompt": "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\nHuman:'"$text2"'\nAI:",
			"temperature": 0.9,
			"max_tokens": 150,
			"top_p": 1,
			"frequency_penalty": 0.0,
			"presence_penalty": 0.6,
			"stop": [" Human:", " AI:"]
		}' | tr '\n' ' ')
END
)
	echo "$output" | tr '\n' ' ' | jq -r 'try .choices[] | .text'
}

chat_bot "$(cat <<-END
$@
END
)"
