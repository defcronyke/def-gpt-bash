#!/bin/bash

chat_marv_qna_bot() {
	text="$@"

	chat_text=$(./chat.sh "$text")
	echo "Chat: $chat_text"
	marv_text=$(./marv.sh "$chat_text")
	echo "Marv: $marv_text"
	qna_text=$(./qna.sh "$marv_text")
	echo "QnA: $qna_text"
}

chat_marv_qna_bot "$@"

